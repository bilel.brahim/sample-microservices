package com.example;

import java.util.stream.Stream;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.messaging.Sink;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.integration.annotation.MessageEndpoint;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@EnableDiscoveryClient
@EnableBinding(Sink.class)
@SpringBootApplication
public class ReservationServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(ReservationServiceApplication.class, args);
	}
}

@RestController
@RefreshScope
class MessageRestController{
	
	@Value("${message}")
	private String message;
	
	@RequestMapping("/message")
	String read(){
		return this.message;
	}
}

@MessageEndpoint
class ReservationProcessor{
	
	@ServiceActivator (inputChannel = "input")
	public void acceptNewReservation(String rn){
		this.reservationRepository.save(new Reservation(rn));
	}
	
	private final ReservationRepository reservationRepository;
	
	@Autowired
	public ReservationProcessor(ReservationRepository reservationRepository) {
		this.reservationRepository = reservationRepository;
	}
}

@Component
class DammyCLR implements CommandLineRunner{

	
	private final ReservationRepository reservationRepository;
	
	@Autowired
	public DammyCLR(ReservationRepository reservationRepository) {
		this.reservationRepository = reservationRepository;
	}
	@Override
	public void run(String... arg0) throws Exception {
		Stream.of("one", "two", "three", "four", "five", "six", "seven").
			forEach(n -> reservationRepository.save(new Reservation(n)));
		
		reservationRepository.findAll().forEach(System.out :: println);
		
	}
	
}
@RepositoryRestResource
interface ReservationRepository extends JpaRepository<Reservation, Long>{
	
}
@Entity
class Reservation{
	
	@Id
	@GeneratedValue
	private Long id;
	
	private String reservationName;
	
	public Reservation() {
	}
	
	public Reservation(String reservationName){
		this.reservationName = reservationName;
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getReservationName() {
		return reservationName;
	}
	public void setReservationName(String reservationName) {
		this.reservationName = reservationName;
	}
	@Override
	public String toString() {
		return "Reservation [id=" + id + ", reservationName=" + reservationName + "]";
	}
}
